# Beer-fest-api

GraphQL Beer-fest endpoint

## Purpose
Provide the backend for beer-fest-app

## Usage

### Install
```
yarn 
```

### Development
```
yarn  dev
```
Goto http://localhost:4444

### Runtime
```
yarn start
```

## Deploy new schema to prisma
```
yarn deploy
```
