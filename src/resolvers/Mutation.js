const Mutations = {
  toggleBeer(parent, args, ctx, info) {
    // firest take a copy of the update
    const updates = { ...args }
    // remove the ID from the updates
    delete updates.id
    // run the update method
    return ctx.db.mutation.updateBeer(
      {
        data: updates,
        where: {
          id: args.id,
        },
      },
      info
    )
  },
  updateBeer(parent, args, ctx, info) {
  // first take a copy of the updates
  const updates = { ...args }
  // remove the ID from the updates
  delete updates.id
  // run the update method
  return ctx.db.mutation.updateBeer(
    {
      data: updates,
      where: {
        id: args.id,
      },
    },
    info
  )
  },
}

module.exports = Mutations
