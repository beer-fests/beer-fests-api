const { forwardTo } = require('prisma-binding')

const Query = {
  beers: forwardTo('db'),
  beer: forwardTo('db'),
  fests: forwardTo('db'),
  fest: forwardTo('db'),
  breweries: forwardTo('db'),
  booths: forwardTo('db'),
  booth: forwardTo('db'),
  users: forwardTo('db'),
}

module.exports = Query
