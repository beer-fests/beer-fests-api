const Subscription = {
  beer: {
    subscribe: (parent, args, ctx, info) => {
      return ctx.db.subscription.beer(
        {
          where: args.where
        },
        info,
      )
    },
  },
}

module.exports = Subscription
