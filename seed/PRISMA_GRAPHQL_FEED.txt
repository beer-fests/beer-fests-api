mutation createUser {
  createUser(data: { name: "Jeroen Knoops", untappd: "johnnybusca" }) {
    id
    name
  }
}

query allUsers {
  users {
    id
    name
  }
}

mutation createFest {
  createFest(
    data: {
      title: "Bier & Big nr 10"
      place: "Eindhoven"
      when: "8 & 9 december"
      date: "2018-12-8"
      active: true
    }
  ) {
    id
    title
  }
}

query allFests {
  fests(where: { active: true }) {
    id
    title
    place
    date
    active
    booths {
      name

      beers {
        name
        brewery {
          name
        }
        collab {
          name
        }
        type
        precentage
        onTap
        special
      }
    }
  }
}

query allBreweries {
  breweries {
    id
    name
  }
}

query allBooths {
  booths {
    id
    name
    beers {
      id
      name
    }
  }
}
query allBeers {
  beers {
    id
    name
    type
    precentage
    brewery {
      name
    }
    booth {
      name
    }
    onTap
    special
  }
}

